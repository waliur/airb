import { css } from '@emotion/core';
import facepaint from 'facepaint';

export default facepaint(['@media(min-width: 0px)', '@media(min-width: 852px)'], {
	literal: true, // (Default: false)
	overlap: false
});

const breakpoints = {
	mobile: 0,
	// tab: 733,
	desktop: 852
};

// https://emotion.sh/docs/media-queries
export const mqPoints = Object.keys(breakpoints).reduce((accumulator, label) => {
	let prefix = typeof breakpoints[label] === 'string' ? '' : 'min-width:';
	let suffix = typeof breakpoints[label] === 'string' ? '' : 'px';
	accumulator[label] = cls =>
		css`
			@media (${prefix + breakpoints[label] + suffix}) {
				${cls};
			}
		`;
	return accumulator;
}, {});

/* 
Usage:
    import { css, jsx } from '@emotion/core';
    import mq from '../../lib/facepaint';
    const section_css = css`
      .container {
        ${mq({
          width: ['100%', '852px'], // [forMobile, forDesktop]
        })}
      }
    `;

*/
