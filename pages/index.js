/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import axios from 'axios';
import mq from '../lib/facepaint';
import { colors } from '../config/theme';

import React from 'react';
import Head from 'next/head';
import moment from 'moment';

import Header from '../components/Header';
import Section from '../components/Section';
import Text from '../components/Text';
import Card from '../components/Card';
import DataGroup from '../components/DataGroup';
import Person from '../components/Person';
import ListTitle from '../components/ListTitle';
import Approver from '../components/Approver';

const { Pair: DataPair } = DataGroup;

export default class extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			request: undefined
		};
	}

	componentDidMount = async () => {
		const request = await axios.get('/data/request1.json').then(res => res.data);
		this.setState({ request });
	};

	render() {
		const { request } = this.state;
		if (!request) return null;
		console.log('xxx', request);
		const { requested_by } = request;

		const appovedRequests = request.approvers.filter(({ status }) => status === 'accepted');
		return (
			<React.Fragment>
				<Head>
					<title>Airbase</title>
				</Head>

				<Section>
					<Header />
					<Text.SecurityMessage className="security_msg">
						Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
						gravida at eget metus.
					</Text.SecurityMessage>
				</Section>

				<Section mobilePadding="0">
					<Card>
						<Card.Header title={request.service.name} iconSrc={request.service.logo} />
						<div css={sideBySide_css}>
							<div className="leftContainer">
								<DataGroup>
									<DataGroup.Pair label="Requested by">
										<Person
											avatarSrc={requested_by.profile_picture}
											name={`${requested_by.first_name} ${requested_by.last_name}`}
										/>
									</DataGroup.Pair>
									<DataGroup.Pair label="Cost">${request.cost_cents / 100}</DataGroup.Pair>
									<DataGroup.InlineRow
										left={
											<DataGroup.Pair label="Renewal Frequency">
												{request.renewal_frequency_in_months} month
											</DataGroup.Pair>
										}
										right={<DataGroup.Pair label="Annual Cost">$720</DataGroup.Pair>}
									/>
									<DataGroup.Pair label="Expense Account">0310 Product Development</DataGroup.Pair>
									<DataGroup.Pair label="File">Receipt-GitHub-Nov.xls</DataGroup.Pair>
									<DataGroup.Pair label="Description" isLong>
										Lorem ipsum dolor sit amet, adipiscing elit. Etiam porta sem malesuada magna
										mollis.
									</DataGroup.Pair>
								</DataGroup>
							</div>
							<div className="rightContainer">
								<ListTitle title="Approved"></ListTitle>
								{appovedRequests.map(({ approver, last_updated_date, status }) => (
									<Approver
										avatarSrc={approver.profile_picture}
										name={approver.first_name + ' ' + approver.last_name}
										email={approver.email}
										subtext={'Approved ' + moment(last_updated_date).format('MMM DD, YYYY')}
										status={status}
									/>
								))}
							</div>
						</div>
					</Card>
				</Section>
			</React.Fragment>
		);
	}
}

const sideBySide_css = css`
	display: flex;
	${mq({
		flexDirection: ['column', 'row']
	})}
	> .leftContainer {
		flex: 100;
		border-right: 1px solid ${colors.grey_light};
		${mq({
			padding: ['0 15px 15px', '15px 15px 15px']
		})}
	}
	> .rightContainer {
		flex: 68;
		${mq({
			padding: ['0 15px 15px', '15px 15px 15px']
		})}
	}
`;
