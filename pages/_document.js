import Document, { Html, Head, Main, NextScript } from 'next/document';
import { css, Global } from '@emotion/core';

class MyDocument extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx);
		return { ...initialProps };
	}

	render() {
		return (
			<Html>
				<Head>
					<link
						rel="stylesheet"
						href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap&text=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.,:;/"
					></link>
				</Head>
				<Global
					styles={css`
						html,
						body {
							margin: 0;
							font-family: 'Roboto';
							font-weight: 400;
							background: #f9fafb;
						}
						p,
						h1,
						h2 {
							margin: 0;
						}
						.clearfix::after {
							display: block;
							content: '';
							clear: both;
						}
					`}
				/>

				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

export default MyDocument;
