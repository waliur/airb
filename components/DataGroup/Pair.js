/** @jsx jsx */
import _ from 'lodash';
import { css, jsx } from '@emotion/core';
import mq from '../../lib/facepaint';
import { colors } from '../../config/theme';
import { body1_css } from '../Text/Text-styles';

const Pair = ({ label, children, isLong }) => (
	<div css={dataPair_css({ isLong })}>
		<div className="label">{label}</div>
		<div className="value">{children}</div>
	</div>
);

const dataPair_css = _.memoize(
	({ isLong }) => css`
		display: flex;
		flex: 1;
		border-bottom: 1px solid ${colors.grey_light};
		${mq({
			flexDirection: [isLong ? 'column' : 'row', 'row']
		})}
		> .label {
			${body1_css};
			color: ${colors.grey};
			width: 122px;
			padding: 15px 15px ${isLong ? 0 : '15px'};
		}
		> .value {
			${body1_css};
			color: ${colors.black};
			display: inline-flex;
			align-items: center;
			flex: 1;
			${mq({
				padding: [`8px 15px 10px ${isLong ? '15px' : 0}`, '10px 15px 10px 0']
			})}
		}
	`
);

export default Pair;
