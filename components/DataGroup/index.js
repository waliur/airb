import DataGroup from './DataGroup';
import Pair from './Pair';
import InlineRow from './InlineRow';

DataGroup.Pair = Pair;
DataGroup.InlineRow = InlineRow;

export default DataGroup;
