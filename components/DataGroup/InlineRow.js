/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import mq from '../../lib/facepaint';
import { colors } from '../../config/theme';
import { body1_css } from '../Text/Text-styles';

const InlineRow = ({ left, right }) => (
	<div css={inlineRow_css}>
		<div className="left">{left}</div>
		<div className="right">{right}</div>
	</div>
);

const inlineRow_css = css`
	display: flex;
	${mq({
		flexDirection: ['column', 'row']
	})}
	.left {
		flex: 1;
		border-right: 1px solid ${colors.grey_light};
		${mq({
			borderWidth: [0, '1px']
		})}
	}
	.right {
		flex: 1;
	}
`;

export default InlineRow;
