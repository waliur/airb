/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import mq from '../../lib/facepaint';
import { colors } from '../../config/theme';

const DataGroup = ({ children }) => <div css={dataGroup_css}>{children}</div>;

const dataGroup_css = css`
	border: 1px solid ${colors.grey_light};
	border-radius: 8px;
	overflow: hidden;
`;

export default DataGroup;
