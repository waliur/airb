/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import mq from '../../lib/facepaint';
import { body1_css } from '../Text/Text-styles';

const Person = ({ avatarSrc, name }) => (
	<div css={person_css}>
		<img className="avatar" src={avatarSrc} alt="person's avatar" />
		<div className="details">
			<p className="name">{name}</p>
		</div>
	</div>
);

const person_css = css`
	display: flex;
	align-items: center;
	> .avatar {
		width: 24px;
	}
	> .details {
		> .name {
			${body1_css};
			margin-left: 8px;
		}
	}
`;

export default Person;
