/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import Checkmark from './Checkmark';
import { body1_css, caption_css } from '../Text/Text-styles';
import { colors } from '../../config/theme';

export default ({ avatarSrc, name, email, status, subtext }) => (
	<div css={approver_css}>
		<span className="serial">1</span>
		<div className="person">
			<img className="avatar" src={avatarSrc} alt="person's avatar" />
			<div className="details">
				<p className="name">
					{name} <span className="email">({email})</span>
				</p>
				<p className="subtext">{subtext}</p>
			</div>
		</div>
		<Checkmark className="checkmark" isChecked={status === 'accepted'} />
	</div>
);

const approver_css = css`
	display: flex;
	flex-direction: row;
	.serial {
		${caption_css};
		height: 15px;
		min-width: 15px;
		text-align: center;
		background-color: ${colors.grey_light};
		border-radius: 50%;
		margin-top: 5px;
		margin-right: 8px;
	}
	> .person {
		flex: 1;
		display: inline-flex;
		flex-direction: row;
		align-items: flex-start;
		.avatar {
			width: 24px;
			border-radius: 50%;
		}
		.details {
			margin-left: 8px;
			.name {
				${body1_css};
			}
			.email {
				${caption_css};
			}
			.subtext {
				${caption_css};
			}
		}
	}
	.checkmark {
		width: 20px;
		height: 20px;
	}
`;
