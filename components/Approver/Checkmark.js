export default ({ isChecked, className }) => {
	if (isChecked) {
		return <img className={className} src="/images/checked-green.png" />;
	} else {
		return <img className={className} src="/images/unchecked.png" />;
	}
};
