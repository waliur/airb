/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import mq from '../../lib/facepaint';

export default () => (
	<div css={header_css}>
		<div className="logo">
			<img
				srcSet="/images/airbase-logo-1x.png 1x, /images/airbase-logo-2x.png 2x"
				src="/images/airbase-logo-1x.png"
				alt="A rad wolf"
			/>
		</div>
	</div>
);

const header_css = css`
	> .logo {
		${mq({
			padding: ['30px 0 20px', '48px 0 20px']
		})}
		.logo-img {
			width: 96px;
		}
	}
`;
