/** @jsx jsx */
import _ from 'lodash';
import { css, jsx } from '@emotion/core';
import mq from '../../lib/facepaint';

export default ({ mobilePadding, children }) => (
	<section css={section_css({ mobilePadding })}>
		<div className="container">{children}</div>
	</section>
);

const section_css = _.memoize(
	({ mobilePadding = '0 15px' }) => css`
		> .container {
			margin: 0 auto;
			box-sizing: border-box;
			${mq({
				width: ['100%', '852px'],
				padding: [mobilePadding, 0]
			})}
		}
	`
);
