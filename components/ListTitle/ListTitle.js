/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { caption_css } from '../Text/Text-styles';

export default ({ title, children }) => (
	<div css={list_css}>
		<span className="title">{title}</span>
		<div>{children}</div>
	</div>
);

const list_css = css`
	.title {
		${caption_css};
		display: inline-block;
		margin-bottom: 15px;
	}
`;
