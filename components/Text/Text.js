/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { caption_css, h2_css, securityMessage_css } from './Text-styles';

const Text = ({ children }) => <p>{children}</p>;
Text.H2 = ({ children }) => <p css={h2_css}>{children}</p>;

Text.Caption = ({ children, className }) => (
	<p css={caption_css} className={className}>
		{children}
	</p>
);

Text.SecurityMessage = ({ children, className }) => (
	<div css={securityMessage_css} className={className}>
		<div className="label">
			<img className="lock_icon" src="/images/lock-orange-2x.png" alt="lock icon" />
			<p className="title">Security Message</p>
		</div>
		<p className="message">{children}</p>
	</div>
);

export default Text;
