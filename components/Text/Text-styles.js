/** @jsx jsx */
import { colors } from '../../config/theme';
import { css, jsx } from '@emotion/core';
import mq from '../../lib/facepaint';
const common_css = css`
	margin: 0;
`;

export const caption_css = css`
	font-size: 12px;
	line-height: 16px;
	color: ${colors.grey};
	${common_css}
`;

export const h2_css = css`
	font-size: 16px;
	font-weight: 400;
	color: ${colors.black};
	${common_css}
`;

export const body1_css = css`
	font-size: 14px;
	font-weight: 400;
	color: ${colors.black};
	${common_css}
`;

export const securityMessage_css = css`
	display: flex;
	font-size: 12px;
	font-weight: 400;
	border-top: 1px solid rgba(34, 34, 34, 0.05);
	${mq({
		flexDirection: ['column', 'row'],
		borderTopWidth: ['1px', 0],
		padding: ['15px 0 25px', '0 0 15px']
	})}
	> .label {
		display: flex;
		align-items: end;
		${mq({
			margin: ['0 0 5px 0', '0 8px 0 0']
		})}

		> .lock_icon {
			width: 9px;
		}
		> .title {
			${caption_css};
			color: ${colors.yellowOrange};
			margin-left: 4px;
		}
	}
	> .message {
		${caption_css};
	}
`;
