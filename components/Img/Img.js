export default ({ className, mobileSrc, tabSrc, desktopSrc }) => (
	<picture>
		{desktopSrc && <source media="(min-width: 852px)" srcSet={desktopSrc} />}
		<img className={className} src={mobileSrc} />
	</picture>
);
