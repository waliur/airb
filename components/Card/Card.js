/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import mq from '../../lib/facepaint';

const Card = ({ children }) => <div css={card_css}>{children}</div>;

const card_css = css`
	background-color: #fff;
	overflow: hidden;
	${mq({
		borderRadius: [0, '10px']
	})}
`;

export default Card;
