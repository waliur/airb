/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { colors } from '../../config/theme';
import mq, { mqPoints } from '../../lib/facepaint';
import { body1_css, h2_css } from '../../components/Text/Text-styles';

const Header = ({
	title,
	iconSrc = 'https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_120,w_120,f_jpg/v1426048404/y4lxnqcngh5dvoaz06as.png'
}) => (
	<div css={header_css}>
		{iconSrc && <img className="logo" src={iconSrc} alt="icon" />}
		{title && <h1 className="title">{title}</h1>}
	</div>
);

const header_css = css`
	padding: 15px 15px;
	display: flex;
	flex-direction: row;
	align-items: center;
	border-bottom: 1px solid ${colors.grey_light};
	${mq({
		borderBottomWidth: [0, '1px']
	})};
	> .logo {
		box-shadow: 0 0 6px 0px rgba(0, 0, 0, 0.25);
		${mq({
			width: ['28px', '35px']
		})};
	}
	> .title {
		color: ${colors.black};
		${mqPoints.mobile(css`
			${body1_css};
			margin: 0 0 0 5px;
		`)}
		${mqPoints.desktop(css`
			${h2_css};
			margin: 0 0 0 10px;
		`)}
	}
`;

export default Header;
