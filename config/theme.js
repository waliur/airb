export const colors = {
	black: 'rgba(34, 34, 34, 1)',
	grey: 'rgba(34, 34, 34, .5)',
	grey_light: 'rgba(34, 34, 34, .05)',
	orange: 'rgba(255, 100, 72, 1)',
	yellowOrange: 'rgba(255, 160, 61, 1)'
};
export default { colors };
